
public class FizzBuzzWhizz {

	private int number;
	
	public String divisibleByThree(int number) {
		setNumber(number);
		if(number % 3 == 0) 
		{
			return "fizz";
		}
		return "Not divisible by 3";
	}
	
	public String divisibleByFive(int number) {
		setNumber(number);
		if(number % 5 == 0) 
		{
			return "buzz";
		}
		return "Not divisible by 5";
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
}
