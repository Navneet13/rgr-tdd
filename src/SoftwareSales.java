
public class SoftwareSales {

    public double calculatePrice(int quantity) {
    	
    	if(quantity<0) {
    	 return -1;	
    	}
    	
    	int subtotal = quantity * 99;
    	double discount = 0;
    	
    	if(quantity>=10 && quantity<=19) {
    		
    		discount = subtotal *.20;
    		
    	}
    	if(quantity>=20 && quantity <=49) {
    		discount = subtotal *.30;
    	}
    	
    	if(quantity>=50 && quantity <=99) {
    		discount = subtotal *.40;
    	}
    	
    	if(quantity>=100) {
    		discount = subtotal * .50;
    	}
    	
    	double finalPrice  = subtotal - discount;
		return finalPrice;
    	
    }

}
