import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SoftwareSalesTest {

	
	SoftwareSales  s;

	@Before
	public void setUp() throws Exception {
		   s= new SoftwareSales();
	}

	@After
	public void tearDown() throws Exception {
	}


	
//	R1:  Price of software is $99 / package
//	R2:  Discount for 10-19 packages is 20%
//	R2a:  Final price for 10-19 is calculated correctly
//	R3:  Discount for 20-49 packages is 30%
//	R3a:  Final price for 20-49 is calculated correctly
//	R4:  Discount for 50-99 packages is 40%
//	R4a:  Final price for 50-99 s calculated correctly
//	R5:  Discount for 100+ packages is 50%
//	R5a:  Final price for 100+ is calculated correctly
//	R6:   If quantity < 0, then return -1

	
	
//	R1:  Price of software is $99 / package
	@Test
	public void testOneSoftwarePurchase() {
		int q = 1;
	    int expectedOutput = 99;
	    assertEquals(expectedOutput, s.calculatePrice(q),0.01);
	}
	
//R2:  Discount for 10-19 packages is 20%
//	R2a:  Final price for 10-19 is calculated correctly
	@Test
	public void test10SoftwarePurchase() {
		int q= 12;
		double expectedOutput = 950.40;
		assertEquals(expectedOutput,s.calculatePrice(q),0.01);
	}
	
	
//	R3:  Discount for 20-49 packages is 30%
//	R3a:  Final price for 20-49 is calculated correctly
	@Test
	public void test20SoftwarePurchase() {
		int q= 30;
		double expectedOutput = 2079;
		assertEquals(expectedOutput,s.calculatePrice(q),0.01);
	}
	
//	R4:  Discount for 50-99 packages is 40%
//	R4a:  Final price for 50-99 s calculated correctly
	@Test
	public void test50SoftwarePurchase() {
		int q= 60;
		double expectedOutput = 3564;
		assertEquals(expectedOutput,s.calculatePrice(q),0.01);
	}
	
//	R5:  Discount for 100+ packages is 50%
//	R5a:  Final price for 100+ is calculated correctly
	@Test
	public void test100PlusSoftwarePurchase() {
		int q= 120;
		double expectedOutput = 5940;
		assertEquals(expectedOutput,s.calculatePrice(q),0.01);
	}
	
//	R6:   If quantity < 0, then return -1
	@Test
	public void testNegativeQuantitySoftwarePurchase() {
		int q = -1;
		double expectedOutput = -1;
		assertEquals(expectedOutput,s.calculatePrice(q),0.01);
	}


}
