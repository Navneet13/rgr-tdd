import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EvenOddTest {

	EvenOdd e;
	int n;

	@Before
	public void setUp() throws Exception {
	  e = new EvenOdd();
	}

	@After
	public void tearDown() throws Exception {
	}
	
//	R1: Accepts 1 integer value, n
//	N > 1  
//	R2: If N < 1, then return false
//	R3: If n is even, return true
//	R4: If n is odd, return false


	//R2: 
	@Test
	public void testNLessThan1() {
		 n = -999;
		assertEquals(false, e.isEven(n));
	}
	
	//R3: 
	
	@Test
	public void testNisEven() {
		n = 20;
		assertEquals(true, e.isEven(n));
	}
	
	//R4:
	@Test
	public void testNisOdd() {
	    n = 27;
		assertEquals(false, e.isEven(n));
	}

}
