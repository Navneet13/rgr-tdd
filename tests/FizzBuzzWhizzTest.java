import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FizzBuzzWhizzTest {

	FizzBuzzWhizz fbw;
	int num;
	String expectedResult;
	
	@Before
	public void setUp() throws Exception {
		fbw = new FizzBuzzWhizz();
	}

	@After
	public void tearDown() throws Exception {
	}

//	R2:Return "buzz" if the number is divisible by 5
//	R3: Return "fizzbuzz" if the number is divisible by 3 or 5 (15)
//	R4: Return the number if no other requirement is fulfilled. The numbers must be returned as a string.
//	R5: If number is prime, return “whizz” 
//	R6 If number meets (R5) AND (R1, R2, or R3) - append “whizz” to end of string
//	Example: 
//	1 returns “1”
//	2 returns “whizz”
//	3 returns “fizzwhizz”
//	4 returns “4”
//	5 returns “buzzwhizz”
//	6 returns “6”
//	7 returns “whizz”
//	9 returns “fizz”
//	10 returns “buzz”
//	15 returns “fizzbuzz”

	
//	R1: Return "fizz" if the number is divisible by 3
	@Test
	public void testDivisibilityByThree() {
		num = 12;
		expectedResult = "fizz";
		assertEquals(expectedResult,fbw.divisibleByThree(num));
	}
	
//	R2:Return "buzz" if the number is divisible by 5
	@Test
	public void testDivisibilityByFive() {
		num = 45;
		expectedResult = "buzz";
		assertEquals(expectedResult,fbw.divisibleByFive(num));

	}

}
